

$(document).ready(function(){
    $(".dropdown").hover(            
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideDown("400");
            $(this).toggleClass('open');        
        },
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideUp("400");
            $(this).toggleClass('open');       
        }
    );
    $("#carousel_custom").carousel({
        interval : 4000,
    });

});

//List of product to show in carousel

var productList = [{
                    name : "Product 1",
                    image: "Product Image 1",
                    price : "$200",
                    description: "Description of Product 1"
                }, 
                {
                    name : "Product 2",
                    image: "Product Image 2",
                    price : "$300",
                    description: "Description of Product 2"
                },
                {
                    name : "Product 3",
                    image: "Product Image 3",
                    price : "$400",
                    description: "Description of Product 3"
                },
                {
                    name : "Product 4",
                    image: "Product Image 4",
                    price : "$500",
                    description: "Description of Product 4"
                }];

// Preparing carousel dynamically
var str = '';
var indicatorStr = '';
for(var i in productList){
    if(i==0){
        str += '<div class="row no-gutter item active"><div class="col-md-8 col-sm-8 col-xs-12">';
        indicatorStr += '<li data-target="#carousel_custom" data-slide-to="0" class="active">1</li>'
    }else{
        str += '<div class="row no-gutter item"><div class="col-md-8">';
        indicatorStr += '<li data-target="#carousel_custom" data-slide-to="'+i+'">'+ (Number(i)+1) +'</li>'
    }
    str += '<img class="carousel-image" src="http://placehold.it/600x400&text='+productList[i].image+'" alt="'+productList[i].image+'" />';
    str += '</div><div class="col-md-4 col-sm-4 col-xs-12 product-detail"><h1 class="title">'+productList[i].name+'</h1>';
    str += '<h4>'+productList[i].price+'</h4><h4>'+productList[i].description+'</h4>';
    str += '<h5><a href="#" class="view-detail">View details <i class="glyphicon glyphicon-play"></i></a></h5>';
    str += '</div></div>';
}
//console.log(indicatorStr);
$("#carousel_inner").append(str) ;
$("#carousel_indicator").append(indicatorStr);
