
var initialValue = 14;
var targetValue = 15;
var totalWidth = 300;
var remainingTargetStr = '';
var elem = document.getElementById("animate");
var currentValue = document.getElementById("currentValue");
var remainingTarget = document.getElementById('remainingTarget');

//** Whether target is achieved or not
if((targetValue - initialValue) > 0){
    remainingTargetStr = '<span class="circle">i</span> You need $';
    remainingTargetStr += (targetValue - initialValue)+' more to reach your target.';
} else{
    remainingTargetStr = '<span class="circle"><i>i</i></span> Target achieved!';
}
remainingTarget.innerHTML = remainingTargetStr; 

var pos = 0;
var id = setInterval(update, 10);
fpos = (initialValue * totalWidth)/targetValue;
function update() {
  if (pos >= fpos) {
    clearInterval(id);
  } else {
    pos ++;
    //elem.style.top = pos + 'px';
    elem.style.width = pos + 'px';
    currentValue.style.left = pos + 'px';
    currentValue.innerHTML = '$' + parseInt(pos / 20);

  }
}